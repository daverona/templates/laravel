#!/bin/ash
set -e

# Set the time zone
if [ ! -z "$APP_TIMEZONE" ]; then
  cp "/usr/share/zoneinfo/$APP_TIMEZONE" /etc/localtime
  echo "$APP_TIMEZONE" > /etc/timezone
  sed -i "s|^date.timezone =.*|date.timezone = $APP_TIMEZONE|" /etc/php7/php.ini
fi

# Set XDEBUG_CONFIG
if [ ! -z "$DEBUG_HOSTNAME" ]; then
  export XDEBUG_CONFIG="remote_host=$DEBUG_HOSTNAME"
fi

if [ "supervisord" == "$(basename $1)" ]; then
  # @see https://laravel.com/docs/7.x/structure#the-storage-directory
  [ ! -f "/app/public/storage" ] && ln -sf /app/storage/app/public /app/public/storage
  composer install
  npm install
fi

exec "$@"
