FROM alpine:3.10 AS asset-builder

# Install dependencies and build
COPY source/ /app/
RUN apk add --no-cache \
    nodejs \
    npm \
  && rm -rf /app/public && mkdir -p /app/public \
  # @see https://laravel.com/docs/7.x/mix
  && cd /app && npm install && npm run production

FROM alpine:3.10

ARG APP_TIMEZONE=UTC
ENV APP_ENV=production
ENV LANG=C.UTF-8

# Install logrotate, nginx, php, python, redis and supervisor
RUN apk add --no-cache \
    composer \
    logrotate \
    nginx \
    nginx-mod-http-headers-more \
    php7 \
    php7-fpm \
    python3 \
    redis \
    tzdata \
  && python3 -m pip install --no-cache-dir --upgrade pip \
  # Install supervisor
  && pip3 install --no-cache-dir supervisor \
  # Set the time zone
  && cp "/usr/share/zoneinfo/$APP_TIMEZONE" /etc/localtime \
  && echo "$APP_TIMEZONE" > /etc/timezone \
  && sed -i "s|^;date.timezone =.*|date.timezone = $APP_TIMEZONE|" /etc/php7/php.ini \
  # Configure cron
  && mkdir -p /var/log/crond \
  # Configure php-fpm
  # @see https://github.com/lsl/docker-nginx-php-fpm/blob/master/php-fpm-www.conf
  && sed -i -e "s|^;pid =|pid =|" \
    -e "s|^;error_log =|error_log =|" \
    -e "s|^;daemonize = yes|daemonize = no|" /etc/php7/php-fpm.conf \
  && sed -i -e "s|^;prefix = /path/to/pools/\$pool|prefix = /var|" \
    -e "s|^listen = 127.0.0.1:9000|listen = /tmp/php7-fpm.sock|" \
    -e "s|^user = nobody|user = nginx|" \
    -e "s|^group = nobody|group = nginx|" \
    -e "s|^;listen.owner = nobody|listen.owner = nginx|" \
    -e "s|^;listen.group = nobody|listen.group = nginx|" \
    -e "s|^;clear_env |clear_env |" \
    -e "s|^;access.log =|access.log =|" \
    -e "s|^;slowlog =|slowlog =|" /etc/php7/php-fpm.d/www.conf \
  # Configure nginx
  && mkdir -p /var/run/nginx \
  # Configure supervisor
  && mkdir -p /var/log/supervisor \
  && echo_supervisord_conf > /etc/supervisord.conf \
  && echo -e "\n[include]\nfiles = /etc/supervisor.d/*.ini" >> /etc/supervisord.conf

# Install php extensions
RUN apk add --no-cache \
    # For mysql dependencies, uncomment the following line:
    php7-pdo_mysql \
    # For sqlite3 dependencies, uncomment the following line:
    # php7-pdo_sqlite \
    # For postgresql dependencies, uncomment the following line:
    # php7-pdo_pgsql \
    php7-bcmath \
    php7-ctype \
    php7-dom \
    php7-fileinfo \
    php7-json \
    php7-mbstring \
    php7-openssl \
    php7-pdo \
    php7-pecl-redis \
    php7-session \
    php7-tokenizer \
    php7-xml \
    php7-xmlwriter

# Install app dependencies
COPY docker/final/logrotate/ /etc/logrotate.d/
COPY docker/final/nginx/ /etc/nginx/conf.d/
COPY docker/final/supervisor/ /etc/supervisor.d/
COPY source/composer.json source/composer.lock /app/
RUN chmod 644 /etc/logrotate.d/* \
  # Note. "composer install" below installs packages and fails when optimizes autoloader.
  # This is expected. Don't worry about it. In the next RUN, "composer dump-autoload" covers this.
  # The reason of this failure is that composer cannot find any class in /app.
  # The reason of not copying ./source to /app is to speed up Docker image build-time.
  && cd /app && (composer install --no-dev --optimize-autoloader || true) && cd - \
  && rm -rf /root/.composer \
  # Add the scheduler to crontabs
  # @see https://laravel.com/docs/7.x/scheduling#introduction
  && echo "* * * * * cd /app && php /app/artisan schedule:run >> /dev/null 2>&1" >> /etc/crontabs/root \
  && mkdir -p /var/log/worker

# Install app
COPY --from=asset-builder /app/package.json /app/package-lock.json /app/
COPY --from=asset-builder /app/public/ /app/public/
COPY source/ /app/
# Note. The following files and directories should not be available in this context.
# The best way to do it is to specify them in .dockerignore.
# source/**/.htaccess
# source/.*
# source/bootstrap/cache/**
# source/node_modules
# source/phpunit.xml
# source/public/storage
# source/README.md
# source/server.php
# source/storage/app/public/**
# source/storage/framework/cache/data/**
# source/storage/framework/sessions/**
# source/storage/framework/testing/**
# source/storage/framework/views/**
# source/storage/logs/**
# source/tests
# source/vendor
RUN cd /app && composer dump-autoload --optimize && cd - \
  # @see https://laravel.com/docs/7.x/installation#configuration
  && chown -R nginx:nginx /app/storage /app/bootstrap/cache \
  # @see https://laravel.com/docs/7.x/deployment
  # Clear application cache
  && php /app/artisan cache:clear \
  # @see https://laravel.com/docs/7.x/views#optimizing-views
  && php /app/artisan view:cache \
  # @see https://laravel.com/docs/7.x/events#event-discovery
  && php /app/artisan event:cache \
  # @see https://laravel.com/docs/7.x/deployment#optimizing-route-loading
  && (php /app/artisan route:cache || true) \
  # @see https://laravel.com/docs/7.x/structure#the-storage-directory
  && ln -sf /app/storage/app/public /app/public/storage \
  && rm -rf /root/.composer

# Configure miscellanea
COPY docker/final/docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh
EXPOSE 80/tcp
WORKDIR /app

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisord.conf", "-n"]
