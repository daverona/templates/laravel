#!/bin/ash
set -e

# Set the time zone
if [ ! -z "$APP_TIMEZONE" ]; then
  cp "/usr/share/zoneinfo/$APP_TIMEZONE" /etc/localtime
  echo "$APP_TIMEZONE" > /etc/timezone
  sed -i "s|^date.timezone =.*|date.timezone = $APP_TIMEZONE|" /etc/php7/php.ini
fi

if [ "supervisord" == "$(basename $1)" ]; then
  # @see https://laravel.com/docs/7.x/deployment#optimizing-configuration-loading
  php /app/artisan config:cache
fi

exec "$@"
