# daverona/laravel

[`PHP7`](https://gitlab.com/daverona/templates/laravel/)
[![pipeline status](https://gitlab.com/daverona/templates/laravel/badges/master/pipeline.svg)](https://gitlab.com/daverona/templates/laravel/-/commits/master)
[![coverage report](https://gitlab.com/daverona/templates/laravel/badges/master/coverage.svg)](https://gitlab.com/daverona/templates/laravel/-/commits/master)

[`PHP5`](https://gitlab.com/daverona/templates/laravel/-/tree/php5)
[![pipeline status](https://gitlab.com/daverona/templates/laravel/badges/php5/pipeline.svg)](https://gitlab.com/daverona/templates/laravel/-/commits/php5)
[![coverage report](https://gitlab.com/daverona/templates/laravel/badges/php5/coverage.svg)](https://gitlab.com/daverona/templates/laravel/-/commits/php5)

This is a repository for Docker images of Laravel application.

* GitLab source repository: [https://gitlab.com/daverona/templates/laravel](https://gitlab.com/daverona/templates/laravel)
* GitLab container registry: [https://gitlab.com/daverona/templates/laravel/container\_registry/](https://gitlab.com/daverona/templates/laravel/container_registry/)

## Prerequisites

* [Docker](https://docs.docker.com/get-docker/) is installed.
* [Traefik](https://docs.traefik.io/) is installed. ([This](http://gitlab.com/daverona/docker-compose/traefik) is recommended.)
* [Dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) is installed. ([This](http://gitlab.com/daverona/docker-compose/dnsmasq) is recommended.)

## Quick Start

### Development

To test-drive the development version:

```bash
cp docker-compose.draft.yml docker-compose.yml
cp .env.draft .env
docker-compose build
docker-compose up -d
```

Wait a few minutes and visit [http://aeon.example/](http://aeon.example/).
If "Laravel" page shows up, it's a success.

(Optional) To skip the image building, do the following instead of `docker-compose build`:

```bash
docker image pull registry.gitlab.com/daverona/templates/laravel:draft
docker image tag registry.gitlab.com/daverona/templates/laravel:draft daverona/laravel:draft
```

### Production

To test-drive the production version:

```bash
cp docker-compose.final.yml docker-compose.yml
cp .env.final .env
docker-compose build
docker-compose up -d
```

Then visit [http://aeon.example/](http://aeon.example/).
If "Laravel" page shows up, it's a success.

(Optional) To skip the image building, do the following instead of `docker-compose build`:

```bash
docker image pull registry.gitlab.com/daverona/templates/laravel:final
docker image tag registry.gitlab.com/daverona/templates/laravel:final daverona/laravel:final
```

## Bootstrapping

To start a project with a clean slate:

```bash
# Stop container
docker-compose down

# Empty source
rm -rf source

# Create a project (specify Laravel version if you will)
# @see https://github.com/composer/docker/issues/30#issuecomment-439906995
docker container run --rm \
  --volume $PWD/source:/source \
  php:7.3.14-alpine3.10 \
  ash -c "\
    apk add --no-cache unzip \
    && wget --quiet --output-document=composer-setup.php https://getcomposer.org/installer \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && cd /source \
    && composer create-project --verbose --prefer-dist laravel/laravel . '7.*' \
    && chown -R $(id -u):$(id -g) /source"
```

`source` contains a brand-new project.  To run, do "Quick Start" section.

## Why Should I Use This?

1. *Fast*: Dockerfiles take as less time as possible to build images once `source` changes.
2. *Small*: Dockerfiles contain no build-time dependencies.
3. *Clean*: `source` directory can serve as project root *without* Docker.
4. *Complete*: Dockerfiles contain exact services required in common projects for each mode.

### Installed Services

#### Development

* Laravel app server (`php artisan serve`)
* Laravel [mix watcher](https://laravel.com/docs/7.x/mix#running-mix)
* Laravel [task scheduler](https://laravel.com/docs/7.x/scheduling) / [queue worker](https://laravel.com/docs/7.x/queues)
* Redis server
* Supervisor (process control)

#### Production

* PHP7-FPM server
* Nginx server
* Laravel [task scheduler](https://laravel.com/docs/7.x/scheduling) / [queue worker](https://laravel.com/docs/7.x/queues)
* Logrotate service
* Redis server
* Supervisor (process control)

## Cloning

Time to make this template yours:

```bash
git clone https://gitlab.com/daverona/templates/laravel.git
cd laravel
rm -rf .git
git init
git add .
git commit -m "Clone daverona/laravel"
```

You have done for a local repository. If you have set up an empty remote repository, say `git@gitserver.com:laravel.git`, set and push to remote:

```bash
git remote add origin git@gitserver.com:laravel.git
git push --set-upstream origin master
```

### Directory Structure

```
docker/                   ; Docker directory                
  draft/                    ; deveopment version
    Dockerfile
    docker-entrypoint.sh
    supervisor/
      app.ini                 ; Laravel app server
      clond.ini               ; Laravel task scheduler (by clond)
      redis.ini               ; Redis server
      supervisor.ini          ; Supervisor
      watcher.ini             ; Laravel mix watcher
      worker.ini              ; Laravel queue worker
  final/                    ; production version
    Dockerfile
    docker-entrypoint.sh
    logrotate/                ; Logrotate configuration
      ...
    nginx/                    ; Nginx configuration
      ...
    supervisor/
      clond.ini               ; Logrotate service and Laravel task scheduler (by crond)
      nginx.ini               ; Nginx server
      php-fpm7.ini            ; PHP7-FPM server
      redis.ini               ; Redis server
      supervisor.ini          ; Supervisor
      worker.ini              ; Laravel queue worker
source/                   ; Laravel project root
  ...
```

## Notes

* `source/.env` is *never* used. Use `.env` instead.

### Development

* Non-macOS/Windows users need to set `DEBUG_HOSTNAME` in `.env` your IP address to use xdebug.

### Production

* The stock `APP_KEY` value should not be used. To generate a new `APP_KEY` value:

```bash
docker container run --rm \
  daverona/laravel:final \
  ash -c "echo 'APP_KEY=' > .env && php /app/artisan key:generate && cat .env"
```

* Before deploy your Laravel app, read these:
  * [Deployment: Optimization](https://laravel.com/docs/7.x/deployment#optimization)
* Distribute the following only:
  * `.env.final`
  * `docker-compose.final.yml`
  * production Docker image

## Advertisement

This is a part of my *Web Framework in Docker* series. Please check out:

* daverona/django: [https://gitlab.com/daverona/templates/django](https://gitlab.com/daverona/templates/django) &mdash; Python
* daverona/laravel: [https://gitlab.com/daverona/templates/laravel](https://gitlab.com/daverona/templates/laravel) &mdash; PHP
* daverona/rails: [https://gitlab.com/daverona/templates/rails](https://gitlab.com/daverona/templates/rails) &mdash; Ruby
* daverona/vue: [https://gitlab.com/daverona/templates/vue](https://gitlab.com/daverona/templates/vue) &mdash; JavaScript

## References

* [https://laravel.com/docs/7.x](https://laravel.com/docs/7.x)

